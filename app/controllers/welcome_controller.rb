class WelcomeController < ApplicationController
 before_action :authenticate_user!
 def new
    @user = User.new
  end

  def add_user
    
    @user = User.new(user_params)
     if @user.save!
     UserMailer.welcome_email(@user).deliver_later
     
       redirect_to root_path
     end
  end

def index
@user=User.all
end

  private

def user_params
  params.require(:user).permit(:email, :password, :password_confirmation)
end

end
