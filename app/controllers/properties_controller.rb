class PropertiesController < ApplicationController
  before_action :set_property, only: [:check_if_payed,:ask_to_pay_tax,:pay_tax,:show, :edit, :update, :destroy]
  load_and_authorize_resource
  before_action :authenticate_user!
  # GET /properties
  # GET /properties.json
  def index
    @properties = Property.all

  end

  # GET /properties/1
  # GET /properties/1.json
  def show
  end


  def ask_to_pay_tax
    UserMailer.send_tax_notice(@property).deliver
    flash[:notice] = "Email has been sent."
    redirect_to property_path(@property)
    
  end
  def pay_tax
    @munth  = @property.Tax
    @quater = @munth*3
    @yearly = @quater*4
  end
  
  def check_if_payed
    @property.payed = true
    UserMailer.tax_payment_ricept(@property).deliver_later
    @property.save
    
    render'show'
  end

  # GET /properties/new
  def new
    @property = Property.new
  end

  # GET /properties/1/edit
  def edit
  end

  # POST /properties
  # POST /properties.json
  def create

    # render plain: params[:property].inspect    
    @property = Property.new(property_params)
    @property.User_id=current_user.id
    @rule=Rule.where(Type_of_property:@property.Type_of_property,Size_of_property:@property.Size_of_property,State:@property.State,District:@property.District).take
    @property.Tax=(@property.Cost*@rule.Tax)/100
    respond_to do |format|
      if @property.save
        format.html { redirect_to @property, notice: 'Property was successfully created.' }
        format.json { render :show, status: :created, location: @property }
      else
        format.html { render :new }
        format.json { render json: @property.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /properties/1
  # PATCH/PUT /properties/1.json
  def update
    respond_to do |format|
      if @property.update(property_params)
        format.html { redirect_to @property, notice: 'Property was successfully updated.' }
        format.json { render :show, status: :ok, location: @property }
      else
        format.html { render :edit }
        format.json { render json: @property.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /properties/1
  # DELETE /properties/1.json
  def destroy
    @property.destroy
    respond_to do |format|
      format.html { redirect_to properties_url, notice: 'Property was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_property
      @property = Property.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def property_params
      params.require(:property).permit(:search,:Owner,:Type_of_property, :Size_of_property, :State, :District, :Cost,:image,:Tax, :User_id,:Address,:longitude,:latitude)
    end
end
