class CheckController < ApplicationController
  def for_super_admin
  	@payed=Property.where(payed:true)
  	@not_payed=Property.where(payed:false)
    check_calculation(@payed,@not_payed)

  end

  def for_supervisor
    @payed=Property.where(payed:true,User_id:current_user.id)
    @not_payed=Property.where(payed:false,User_id:current_user.id)
    check_calculation(@payed,@not_payed)
  end
  def search
  	
     @Property=Property.search(params[:search])
     	@payed=@Property.where(payed:true)
     	@not_payed=@Property.where(payed:false)
      check_calculation(@payed,@not_payed)

  end
  private
  def check_calculation(payed,not_payed)
    @payed = payed
    @not_payed=not_payed
    @a=0
    @payed.each do |i| @a += i.Tax end 
    @b=0
    @not_payed.each do |i| @b += i.Tax end 
    
  end
end
