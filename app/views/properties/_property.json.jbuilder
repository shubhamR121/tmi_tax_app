json.extract! property, :id, :Type_of_property, :Size_of_property, :State, :District, :Cost, :Tax, :User_id, :created_at, :updated_at
json.url property_url(property, format: :json)
