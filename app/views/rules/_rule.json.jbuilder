json.extract! rule, :id, :Type_of_property, :Size_of_property, :State, :District, :frequency, :Tax, :created_at, :updated_at
json.url rule_url(rule, format: :json)
