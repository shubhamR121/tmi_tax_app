class Rule < ApplicationRecord
	validates :Type_of_property, :Size_of_property, :State, :District, :frequency, :Tax, presence: true
end
