class Property < ApplicationRecord
  belongs_to :User
  has_one_attached :image
  # geocoded_by :Address
  # after_validation :geocode
  validates :Owner,:Type_of_property, :Size_of_property, :State, :District, :Cost,:image,:Tax, :User_id,:Address, presence: true

 def self.search(search)
 	if search
 		@Property=Property.where(District:search)
 	else
    	@Property=Property.all
    end

 end
 def self.search1(search1)
 	if search1
 		search1=DateTime.parse(search1)
 		rang = search1.beginning_of_year..search1.end_of_year
 	    @Properties=Property.where(created_at:rang)
 	else
 		@Properties=Property.all
 	end
 end

end
