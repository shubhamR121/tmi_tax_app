class ApplicationMailer < ActionMailer::Base
  default from: 'Tax_Co-orporation.com'
  layout 'mailer'
end
