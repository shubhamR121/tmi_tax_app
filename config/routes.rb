Rails.application.routes.draw do
  

  get'tax_collected_in_the_year/check_the_year'

 #routes to check the admin and to get the data according to the city
  get 'check/for_super_admin'
  get 'check/search'
  get 'check/for_supervisor'
  
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin' #routs for the admin pannel
  
  resources :rules,:properties #for all models
  


  #for the admin to add new users
  get  'employees/new',  to: 'welcome#new'
  post 'employees',  to: 'welcome#add_user'
  get  'users' ,to: 'welcome#index'
  
  devise_for :users  #fro the devise gem 
  
  #buttens for propwerty tax paied or not 
  post 'properties/pay_tax/:id' ,to: 'properties#pay_tax'
  post 'properties/check_if_payed/:id' ,to: 'properties#check_if_payed'
 
  # send tax demand notice button  
  post 'properties/:id' ,to: 'properties#ask_to_pay_tax'
 
   # for the home page 
  get 'home/index'
  root 'home#index'

end
