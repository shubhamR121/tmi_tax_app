class AddpayedColumnToProperties < ActiveRecord::Migration[6.0]
  def change
  	 add_column :properties, :payed, :boolean,default: false
  end
end
