class CreateRules < ActiveRecord::Migration[6.0]
  def change
    create_table :rules do |t|
      t.string :Type_of_property
      t.string :Size_of_property
      t.string :State
      t.string :District
      t.integer :frequency
      t.float :Tax

      t.timestamps
    end
  end
end
