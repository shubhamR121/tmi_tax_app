class CreateProperties < ActiveRecord::Migration[6.0]
  def change
    create_table :properties do |t|
      t.string :Type_of_property
      t.string :Size_of_property
      t.string :State
      t.string :District
      t.float :Cost
      t.float :Tax
      t.references :User, null: false, foreign_key: true

      t.timestamps
    end
  end
end
