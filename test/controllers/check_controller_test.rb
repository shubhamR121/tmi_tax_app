require 'test_helper'

class CheckControllerTest < ActionDispatch::IntegrationTest
  test "should get for_super_admin" do
    get check_for_super_admin_url
    assert_response :success
  end

  test "should get for_supervisor" do
    get check_for_supervisor_url
    assert_response :success
  end

end
