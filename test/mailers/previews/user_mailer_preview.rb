# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
 def welcome_email
    # Set up a temporary order for the preview
    @user = User.new( email: "sahil_sahikh@gmail.com",password: 123456, password_confirmation: 123456) 

    UserMailer.welcome_email(@user)
 end
 def send_tax_notice
     @user = Property.first
  	 UserMailer.send_tax_notice(@user)
  end
   def tax_payment_ricept
  	@user = Property.first
  	 UserMailer.tax_payment_ricept(@user)
  end  
end

# http://localhost:3000/rails/mailers/user_mailer/send_tax_notice
